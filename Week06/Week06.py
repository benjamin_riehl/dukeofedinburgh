import time 
from Adafruit_MCP230xx import Adafruit_MCP230XX
mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

output = mcp.OUTPUT

yellow = 1

mcp.config(1, output)

mcp.output(1, 1)

time.sleep(1)

mcp.output(1, 0)

