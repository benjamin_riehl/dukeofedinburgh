from Lib7seg import *
from Adafruit_MCP230xx import Adafruit_MCP230XX
import time

mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

mcp.pullup(8, 1)
mcp.pullup(9, 1)

MCPsetup()
MCPstop()

z = 1
y = 1
x = 0

while True:
	z = mcp.input(9) >> 9  #Down
	y = mcp.input(8) >> 8  #Up
	if z == 0:  #Down is pressed
		x = x - 1
		MCPstop()
		if x == -1:
			x = 9
		
	elif y == 0: #Up is pressed
		x = x + 1
		MCPstop()
		if x == 10:
			x = 0
	
	#----Displays the number on the display----
	elif x == 0:
		zero1()
	elif x == 1:
		one1()
	elif x == 2:
		two1()
	elif x == 3:
		three1()
	elif x == 4:
		four1()
	elif x == 5:
		five1()
	elif x == 6:
		six1()
	elif x == 7:
		seven1()
	elif x == 8:
		eight1()
	elif x == 9:
		nine1()
	time.sleep(.2)    #Adjust this to how long it takes to fully press a button
	
