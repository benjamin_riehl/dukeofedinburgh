import sys
if sys.version_info[0] < 3:
    raise Exception("Must be using Python 3")

####That checks to see if the code is running in python3 and errors out if it detects a lower version
import rpisensors
import time
import board
import busio
import adafruit_character_lcd.character_lcd_rgb_i2c as character_lcd
 
sensor = rpisensors.BMP180(1)
 
#Setup for the LCD
lcd_columns = 16
lcd_rows = 2
i2c = busio.I2C(board.SCL, board.SDA)
lcd = character_lcd.Character_LCD_RGB_I2C(i2c, lcd_columns, lcd_rows)
lcd.clear()
lcd.cursor = True
lcd.blink = True


print ("Please do not touch the sensor for the next 15 seconds")
print ("Getting Ambient Air temperature")
print ("Please wait...")
lcd.message = "DON'T TOUCH FOR\n 15 SECONDS!"
time.sleep(10)
ambi = sensor.read_temperature()
print ("The ambiant temperature is:")
print (ambi)
lcd.clear()

try:
	while True:
		temp = sensor.read_temperature()
		tempp = temp - ambi
		temp = 'Temperature:\n' + str(temp)
		lcd.message = temp
		time.sleep(.5)
		lcd.clear()
		if tempp <= 0.7:
			lcd.color = [0, 0, 100]
		elif tempp >= 0.9: 
			lcd.color = [100, 0, 0]

except KeyboardInterrupt:
	lcd.clear()
	lcd.message = 'Bye!'
	time.sleep(5)
	lcd.cursor = False
	lcd.blink = False
	lcd.clear()
	lcd.color = [0, 0, 0]
	
