import rpisensors
import sys
import datetime
import gspread
import time
import character_lcd_rgb_but as LCD
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = ServiceAccountCredentials.from_json_keyfile_name('gspread.json', scope) # TODO: Fix this

sensor = rpisensors.BMP180(1)
 
#Setup for the LCD
lcd = LCD.Adafruit_CharLCDPlate()
lcd.clear()
lcd.cursor = True
lcd.blink = True

print ("Please do not touch the sensor for the next 15 seconds")
print ("Getting Ambient Air temperature")
print ("Please wait...")
lcd.message ('DO NOT TOUCH FOR\n 15 SECONDS!')
time.sleep(10)
ambi = sensor.read_temperature()
print ('The ambiant temperature is:')
print (ambi)
lcd.clear()
lcd.set_color(0, 0, 0)
menu = 0
temp = 0
pa = 0
err = 0

def loginGspread():
    try:
        gc = gspread.authorize(credentials)
        print ("Login successful")
    except:
        print ("Unable to login. Check your key, it might be broken")
        cleanuplcd()

def opensheet():
    try:
        wks = gc.open(bmp).sheet1
        return worksheet
    except:
        print ("Unable to get spreadsheet: bmp.sheet1")
        cleanuplcd()

def logreading():
    try:
        wks.append_row((datetime.datetime.now(), temp, pa))
    except:
        if err <= 5:
            print 'Something is broke, try again!'
            err = err + 1
        elif err >= 6:
            print 'Something is actually broke...'
            cleanuplcd()
        worksheet = None


def cleanuplcd():
    time.sleep(.5)
    lcd.clear()
    time.sleep(.5)
    lcd.message = ('Bye!')
    time.sleep(5)
    lcd.cursor = False
    lcd.blink = False
    lcd.clear()
    lcd.set_color(0, 0, 0)
    exit()

try:
    loginGspread()
    opensheet()
    while True:
        temp, pa = sensor.read_temperature_and_pressure()
        tempp = temp - ambi
        if menu == 0:
            temp = ('Temperature:\n' + str(temp))
            lcd.message (temp)
        elif menu == 1:
            pa = ('Pressure:\n' + str(pa))
            lcd.message (pa)
        time.sleep(.5)
        lcd.clear()
        if tempp <= 0.7:
            lcd.set_color(0, 0, 100)
        elif tempp >= 0.9:
            lcd.set_color(100, 0, 0)
        if lcd.is_pressed(LCD.UP):
            menu = 1
        elif lcd.is_pressed(LCD.DOWN):
            menu = 0
        elif lcd.is_pressed(LCD.SELECT):
            cleanuplcd()
        elif lcd.is_pressed(LCD.LEFT):
            logreading()

except KeyboardInterrupt:
    cleanuplcd()
