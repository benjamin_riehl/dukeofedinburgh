#!/usr/bin/env python

import RPi.GPIO as io 
import time 
from random import uniform #Random selection of a decimal number 
from sys import exit

buttons = [15, 13] #15 is the right button, 13 is the left button 
led = 40      

io.setmode (io.BOARD)  
io.setup(buttons, io.IN, pull_up_down=io.PUD_UP)
io.setup(led, io.OUT)

io.output (led, 0)
time.sleep(uniform(3, 10))
io.output (led, 1)

while True:
	if io.input(15) == 0:
		print("Right side won!")
		io.cleanup()
		exit()
	elif io.input(13) == 0:
		print("Left side won!")
		io.cleanup()
		exit()
