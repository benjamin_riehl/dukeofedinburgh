from time import sleep
sleep(30)

import datetime as dt
import RPi.GPIO as GPIO
import os
import rpisensors

os.system ("echo 'Hello!' | lpr")
os.system ("echo 'Camera is ready!' | lpr")

sensor = rpisensors.BMP180(1)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP) #Pin for the button
GPIO.setup(27, GPIO.OUT)                          #Pin for LED on the button
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP) #Pin for the shutdown button!

while True:
    GPIO.output(27, 1)
    sleep(.5)
    print_state = GPIO.input(17)
    power_state = GPIO.input(22)
    if print_state == False:
        try:
            GPIO.output(27, 0)
            temp, pa = sensor.read_temperature_and_pressure()
            temp = str(temp)
            temp = str('Temperature: ') + temp + str(' C')
            sleep(1)

            commandd = "echo '{}' | lpr".format(temp)
            os.system(commandd)
            sleep(1)

            # !!!!!!!!PRINTER TIME!!!!!!
            commandd2 = 'raspistill -n -t 200 -w 512 -h 384 -ae 36,0x00,0x8080FF -a "{}" -o - | lp'.format(temp)
            os.system (commandd2)
            sleep(5)
        except Traceback as err:
            print err
    elif power_state == False:
        os.system("echo 'Powering off, please wait 30 seconds before removing power' | lpr")
        sleep(5)
        os.system("sudo sync && sudo halt")