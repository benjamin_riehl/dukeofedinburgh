import rpisensors
import os
import sys
import datetime
import gspread
import time
import character_lcd_rgb_but as LCD
from oauth2client.service_account import ServiceAccountCredentials

#os.chrdir('/home/pi/duke/Week19')     #Change this to your Week19.py's working directory

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/duke/Week19/gspread.json', scope)

sensor = rpisensors.BMP180(1)

#Setup for the LCD
lcd = LCD.Adafruit_CharLCDPlate()
lcd.clear()
lcd.cursor = True
lcd.blink = True

try:
    gc = gspread.authorize(creds)
    print ("Login successful")
    wks = gc.open("bmp").sheet1
except:
    print ("Unable to login. Check your key, it might be broken")
    cleanuplcd()
print ("Please do not touch the sensor for the next 15 seconds")
print ("Getting Ambient Air temperature")
print ("Please wait...")
lcd.message ('DO NOT TOUCH FOR\n 15 SECONDS!')
time.sleep(10)
ambi = sensor.read_temperature()
print ('The ambiant temperature is:')
print (ambi)
lcd.clear()
lcd.set_color(0, 0, 0)

menu = 0
temp = 0
pa = 0

def logreading():
    wks.append_row((tempz, paz, timez))

def cleanuplcd():
    time.sleep(.5)
    lcd.clear()
    time.sleep(.5)
    lcd.message = ('Bye!')
    time.sleep(5)
    lcd.cursor = False
    lcd.blink = False
    lcd.clear()
    lcd.set_color(0, 0, 0)
    exit()

try:
    while True:
        temp, pa = sensor.read_temperature_and_pressure()
        tempp = temp - ambi
        tempz = temp
        paz = pa
        if menu == 0:
            temp = ('Temperature:\n' + str(temp))
            lcd.message (temp)
        elif menu == 1:
            pa = ('Pressure:\n' + str(pa))
            lcd.message (pa)
        time.sleep(.5)
        lcd.clear()
        if tempp <= 0.7:
            lcd.set_color(0, 0, 100)
        elif tempp >= 0.9:
            lcd.set_color(100, 0, 0)
        if lcd.is_pressed(LCD.UP):
            menu = 1
        elif lcd.is_pressed(LCD.DOWN):
            menu = 0
        elif lcd.is_pressed(LCD.SELECT):
            cleanuplcd()
        elif lcd.is_pressed(LCD.LEFT):
            timezz = datetime.datetime.now()
            timez = str(timezz)
            logreading()

except KeyboardInterrupt:
    cleanuplcd()
