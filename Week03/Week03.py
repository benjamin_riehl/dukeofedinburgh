#!/usr/bin/env python

import RPi.GPIO as io 
import time 
import signal            

io.setmode (io.BOARD)  
io.setup(15,io.IN,pull_up_down=io.PUD_UP)

try:
	while True:
		print io.input(15)
		time.sleep(.1)

except KeyboardInterrupt:
	print("Stopping program and Cleaning it up")
	time.sleep(1)                              # Making it wait so i can see the message
	io.cleanup()
