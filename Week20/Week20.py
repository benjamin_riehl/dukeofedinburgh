from picamera import PiCamera
import time

camera = PiCamera()
camera.rotation = 180

# High quality image
camera.resolution = (2592, 1944)
camera.framerate = 15
camera.capture('/home/pi/high_image.jpg')

time.sleep(5)

# Very low quality image
camera.resolution = (64, 64)
camera.capture('/home/pi/garbage_image.jpg')