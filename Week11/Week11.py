import rpisensors
import time
sensor = rpisensors.BMP180(1)

sensor.verify()  # This is to see if the sensor is detected

temp, pa = sensor.read_temperature_and_pressure()

while True:
	temp, pa = sensor.read_temperature_and_pressure()
	print temp
	print pa
	time.sleep(.5)
