import datetime as dt
import RPi.GPIO as GPIO
import os
from time import sleep
import rpisensors
from pydrive.auth import GoogleAuth       # Import libraries for Google Drive
from pydrive.drive import GoogleDrive

sensor = rpisensors.BMP180(1)

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#Authentication for Google Drive. Yuck!
gauth = GoogleAuth()
gauth.CommandLineAuth()
drive = GoogleDrive(gauth)

while True:
    sleep(.5)
    drive_state = GPIO.input(18)
    print_state = GPIO.input(17)
    if drive_state == False:
        file_title = dt.datetime.now().strftime('%m-%d-%H:%M') + str('.jpg') # Set the title for the picture

        temp, pa = sensor.read_temperature_and_pressure()
        temp = str(temp)
        temp = str('Temperature: ') + temp + str(' C')

        print temp

        commande = 'raspistill -vf -hf -ae 45,0x00,0x8080FF -a "{}" -o '.format(temp) + file_title
        os.system(commande)

        # !!!!!GOOGLE TIME!!!!!

        gdrive = drive.CreateFile()
        gdrive.SetContentFile(file_title)
        gdrive.Upload()
        print("File uploaded successfully!")
        sleep(5)

    elif print_state == False:
        temp, pa = sensor.read_temperature_and_pressure()
        temp = str(temp)
        temp = str('Temperature: ') + temp + str(' C')
        sleep(1)

        commandd = "echo '{}' | lpr".format(temp)
        os.system(commandd)
        sleep(1)

        # !!!!!!!!PRINTER TIME!!!!!!
        commandd2 = 'raspistill -vf -hf -n -t 200 -w 512 -h 384 -ae 36,0x00,0x8080FF -a "{}" -o - | lp'.format(temp)
        os.system (commandd2)
        sleep(5)

