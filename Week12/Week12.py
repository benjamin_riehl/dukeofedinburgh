import rpisensors
import time
import RPi.GPIO as GPIO

sensor = rpisensors.BMP180(1)

print sensor.verify()  # This is to see if the sensor is detected

red = 17
blue = 27

GPIO.setmode(GPIO.BCM)
GPIO.setup(red, GPIO.OUT)
GPIO.setup(blue, GPIO.OUT)

temp = sensor.read_temperature()

# ----- Get Ambiant Temperature

print "Please do not touch the sensor for the next 15 seconds"
print "Getting Ambient Air temperature"
print "Please wait..."
time.sleep(10)
ambi = sensor.read_temperature()
print "The ambiant temperature is:"
print ambi


while True:
	temp = sensor.read_temperature()
	tempp = temp - ambi
	# print tempp      # remove the # if you want to see the tempp displayed #
	
	if tempp <= 0.6:
		GPIO.output(blue, GPIO.HIGH)
		GPIO.output(red, GPIO.LOW)
		print "Cold"
	elif tempp >= 0.7: 
		GPIO.output(red, GPIO.HIGH)
		GPIO.output(blue, GPIO.LOW)
		print "Warm"
	
	time.sleep(.5)
