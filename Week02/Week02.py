import RPi.GPIO as io 
import time              #These two are the needed librarys to blink an led
import signal            # To make the program stop cleanly

io.setmode (io.BOARD)  #Use physical pin numbering
io.setup(40,io.OUT)

try:
	while True:
		io.output(40,1) #turn LED on
		time.sleep(1)   #Wait one second
		io.output(40,0) #turn LED off
		time.sleep(1)   #Wait one second
		
except KeyboardInterrupt:
	print("Stopping program and Cleaning it up")
	time.sleep(1)                              # Making it wait so i can see the message
	io.cleanup()
