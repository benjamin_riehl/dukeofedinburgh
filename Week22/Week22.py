import datetime as dt
from picamera import PiCamera
from time import sleep

from pydrive.auth import GoogleAuth       # Import libraries for Google Drive
from pydrive.drive import GoogleDrive

file_title = dt.datetime.now().strftime('%m-%d-%H:%M:%S') + str('.jpg') # Set the title for the picture

camera = PiCamera()
camera.rotation = 180

# High quality image
camera.resolution = (2592, 1944)
camera.framerate = 15
camera.capture(file_title)

file_loc = str('/home/pi/duke/Week22/') + file_title

gauth = GoogleAuth()
gauth.CommandLineAuth() # Creates local webserver and auto handles
#authentication.
drive = GoogleDrive(gauth)

gdrive = drive.CreateFile()
gdrive.SetContentFile(file_title)
gdrive.Upload()

