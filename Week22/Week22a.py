import datetime as dt
import RPi.GPIO as GPIO
from picamera import PiCamera
from time import sleep
import rpisensors
from pydrive.auth import GoogleAuth       # Import libraries for Google Drive
from pydrive.drive import GoogleDrive

sensor = rpisensors.BMP180(1)

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#Authentication for Google Drive. Yuck!
gauth = GoogleAuth()
gauth.CommandLineAuth()
drive = GoogleDrive(gauth)

camera = PiCamera()
camera.rotation = 180
camera.resolution = (2592, 1944)
camera.annotate_text_size = 120
camera.framerate = 15
camera.led = False

while True:
    sleep(.5)
    input_state = GPIO.input(18)
    if input_state == False:
        file_title = dt.datetime.now().strftime('%m-%d-%H:%M:%S') + str('.jpg') # Set the title for the picture

        temp, pa = sensor.read_temperature_and_pressure()
        temp = str(temp)
        temp = str('Temperature: ') + temp + str(' C')

        print temp

        camera.led = True
        camera.annotate_text = temp
        camera.capture(file_title)
        print("Picture has been taken!")
        camera.led = False
        # !!!!!GOOGLE TIME!!!!!

        gdrive = drive.CreateFile()
        gdrive.SetContentFile(file_title)
        gdrive.Upload()
        print("File uploaded successfully!")
        sleep(5)