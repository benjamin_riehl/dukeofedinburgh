from Adafruit_MCP230xx import Adafruit_MCP230XX
mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

output = mcp.OUTPUT
out = mcp.output


def MCPsetup():
	output = mcp.OUTPUT
	out = mcp.output
	
	mcp.config(1, output)
	mcp.config(2, output)
	mcp.config(3, output)
	mcp.config(4, output)
	mcp.config(5, output)
	mcp.config(6, output)
	mcp.config(7, output)

def MCPstop():
	mcp.output(1, 0)
	mcp.output(2, 0)
	mcp.output(3, 0)
	mcp.output(4, 0)
	mcp.output(5, 0)
	mcp.output(6, 0)
	mcp.output(7, 0)

# All of this chicken scratch is just the numbers for the 7-segment display...
def zero1():
	out(1, 1)
	out(2, 1)
	out(3, 1)
	out(4, 1)
	out(5, 1)
	out(6, 1)
def zero0():
	out(2, 0)
	out(3, 0)
	out(4, 0)
	out(5, 0)
	out(6, 0)
	out(7, 0)
def one1():
	out(1, 1)
	out(4, 1)
def one0():
	out(1, 0)
	out(4, 0)
def two1():
	out(2,1)
	out(3,1)
	out(4,1)
	out(5,1)
	out(7,1)
def two0():
	out(2,0)
	out(3,0)
	out(4,0)
	out(5,0)
	out(7,0)
def three1():
	out(1,1)
	out(2,1)
	out(4,1)
	out(5,1)
	out(7,1)
def three0():
	out(1,0)
	out(2,0)
	out(4,0)
	out(5,0)
	out(7,0)
def four1():
	out(1,1)
	out(4,1)
	out(6,1)
	out(7,1)
def four0():
	out(1,0)
	out(4,0)
	out(6,0)
	out(7,0)
def five1():
	out(1,1)
	out(2,1)
	out(5,1)
	out(6,1)
	out(7,1)
def five0():
	out(1,0)
	out(2,0)
	out(5,0)
	out(6,0)
	out(7,0)
def six1():
	out(1,1)
	out(2,1)
	out(3,1)
	out(5,1)
	out(6,1)
	out(7,1)
def six0():
	out(1,0)
	out(2,0)
	out(3,0)
	out(5,0)
	out(6,0)
	out(7,0)
def seven1():
	out(1,1)
	out(4,1)
	out(5,1)
def seven0():
	out(1,0)
	out(4,0)
	out(5,0)
def eight1():
	out(1,1)
	out(2,1)
	out(3,1)
	out(4,1)
	out(5,1)
	out(6,1)
	out(7,1)
def eight0():
	out(1,0)
	out(2,0)
	out(3,0)
	out(4,0)
	out(5,0)
	out(6,0)
	out(7,0)
def nine1():
	out(1,1)
	out(2,1)
	out(4,1)
	out(5,1)
	out(6,1)
	out(7,1)
def nine0():
	out(1,0)
	out(2,0)
	out(4,0)
	out(5,0)
	out(6,0)
	out(7,0)
